package jp.alhinc.hashiura_miki.fizzbuzz;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



public class FizzBuzzSampleTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void print_01() {
		assertThat(FizzBuzzSample.calc(15),is("FuzzBuzz"));
	}

	@Test
	public void print_02() {
		assertThat(FizzBuzzSample.calc(3),is("Fuzz"));
	}

	@Test
	public void print_03() {
		assertThat(FizzBuzzSample.calc(5),is("Buzz"));
	}

	@Test
	public void print_04() {
		assertThat(FizzBuzzSample.calc(2),is("2"));
	}


}
