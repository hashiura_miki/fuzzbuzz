package jp.alhinc.hashiura_miki.fizzbuzz;

public class FizzBuzzSample {

	public static void main(String[] args) {
		for( Integer i = 0; i < 20 ; i++)
			System.out.println(calc(i));
	}

	public static String calc(Integer num) {
		if(num % 3 == 0 && num % 5 == 0) {
			return "FuzzBuzz";
		}
		else if(num % 3 == 0) {
			return "Fuzz";
		}
		else if(num % 5 == 0) {
			return "Buzz";
		}
		else {
			return String.valueOf(num);
		}
	}
}
